import csv
import serial

ser = serial.Serial('COM14', 9600, timeout=1)
if ser.is_open:
    print('Serial port open.')
else:
	print('Serial port is not open.')

with open('serial_output.csv', 'wb') as csvfile:
    print('File open.')
    while ser.is_open:
        line = ser.readline()
        csvfile.write(line)
        print(line)
    print('Serial port closed.')
print('File closed.')
