"""
These are the classes that will be used to hold pertinent test information and
data for the Salt Spray chambers at NSWCCD.  The goal is to create classes and
functions needed to define tests, log relevant information, and display test
analytics.
"""

import datetime
import logging
import time

import kivy.clock
import kivy.event
import kivy.logger
import kivy.properties
import kivy.uix.button
import kivy.uix.gridlayout
import kivy.uix.label
import kivy.uix.textinput

import helpers

class EnvironmentTest(kivy.uix.gridlayout.GridLayout):
    """
    This is the main class for running a test.
    """
    def __init__(self, container, test_name, **kwargs):
        super(EnvironmentTest, self).__init__(**kwargs)
        self.container = container
        self.elapsed_time = datetime.timedelta()
        self.name = test_name
        self.start_time = datetime.datetime.now()
        kivy.logger.Logger.info(
            "OBJECTS: {0} - Added new test [{1}] to [{2}].".format(
                time.strftime('%X %x'), self.name, self.container))
        self.label = kivy.uix.label.Label(
            text="{0}\nStart Date: {1}-{2}-{3}\nElapsed Time: {4}".format(
                self.name, self.start_time.month, self.start_time.day,
                self.start_time.year, self.elapsed_time),
            color=helpers.change_color('normal'))
        self.add_widget(self.label)

        kivy.clock.Clock.schedule_interval(self.test_callback, 1)
        self.create_dispatcher()

        self.logger = helpers.setup_logger(
            self.name,
            "./logs/tests/{0}-{1}-{2}.txt".format(
                                                  datetime.date.today(),
                                                  self.container,
                                                  self.name),
            level=logging.INFO)

    def create_dispatcher(self):
        self.dispatcher = TestDispatcher()
        self.dispatcher.bind(running=self.update_color)

    def start_test(self):
        self.dispatcher.running = True

    def stop_test(self):
        self.dispatcher.running = False

    def test_callback(self, dt):
        if self.dispatcher.running:
            self.elapsed_time += datetime.timedelta(seconds=1)
        self.label.text = "{0}\nStart Date: {1}-{2}-{3}\nElapsed Time: " \
            "{4}".format(
            self.name, self.start_time.month, self.start_time.day,
            self.start_time.year, self.elapsed_time)
        # kivy.logger.Logger.debug(
        #     "TESTS: {0} - Test callback for [{1}]. Elapsed time: [{2}]".format(
        #         time.strftime('%X %x'), self.name, self.elapsed_time))

    def update_color(self, dispatcher, running):
        if running:
            self.label.color = helpers.change_color('normal')
        else:
            self.label.color = helpers.change_color('error')

    def write_log(self, container, dt):
        log = ""
        for sensor in container.ids.values():
            log += "{0}, ".format(sensor.sensor.state)
        log += str(self.dispatcher.running)
        self.logger.info(log)

    def write_log_header(self, container):
        log = ""
        for sensor in container.ids.values():
            log += "{0} ({1}), ".format(sensor.sensor.parameter,
                                        sensor.sensor.unit)
        log += "Running (bool)"
        self.logger.info(log)

class TestDispatcher(kivy.event.EventDispatcher):
    """
    This class contains the event dispatchers for running tests.
    """
    running = kivy.properties.BooleanProperty()

    def __init__(self, **kwargs):
        super(TestDispatcher, self).__init__(**kwargs)
        self.running = True
