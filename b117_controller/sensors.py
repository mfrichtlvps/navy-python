"""
This file contains the sensor classes and helper functions for the cabinet
controller application.
Current helper functions are:
    change_color: Used to change item colors indicating their current status.
"""

import time
import kivy.clock
import kivy.event
from kivy.logger import Logger
import kivy.properties
import kivy.uix.boxlayout
import kivy.uix.button
import kivy.uix.label
import helpers

global_fault = False

class SensorItem(kivy.uix.label.Label):
    """
    This is the generic class for sensor items, which can be any type of sensor or other item for use with the monitor application.  It holds the sensor
    state, which is represented as an EventDispatcher class.
    """

    def __init__(self, reading, **kwargs):
        super(SensorItem, self).__init__(**kwargs)
        self.color = helpers.change_color('normal')
        self.fault = False
        self.global_fault = False
        self.create_sensor(reading)
        self.text = self.sensor.parameter + " " + str(self.sensor.state) + " " + self.sensor.unit

    def create_sensor(self, reading):
        Logger.info("OBJECTS: {0} - Creating sensor [{1}] for [{2}]".format(time.strftime('%X %x'), reading['sensor_id'], reading['parameter']))
        if reading['unit'] == 'boolean':
            self.sensor = SensorDigital(reading)
        else:
            self.sensor = SensorAnalog(reading)
        self.ids['my_sensor'] = self.sensor
        self.sensor.bind(state=self.update_text)
        self.sensor.bind(fault=self.update_fault)
        self.sensor.update(reading['measurement'])

    def update(self, new_state):
        self.sensor.update(new_state)

    def update_fault(self, sensor, fault):
        if fault:
            self.color = helpers.change_color('error')
            self.fault = True
            Logger.debug("OBJECTS: {0} - [{1}] fault.".format(time.strftime('%X %x'), sensor.sensor_id))
        else:
            self.color = helpers.change_color('normal')
            self.fault = False

    def update_text(self, sensor, reading):
        self.text = "{0} {1} {2}".format(sensor.parameter, str(reading), sensor.unit)


class SensorDigital(kivy.event.EventDispatcher):
    """
    Class for digital sensors & items, such as power relays.  Creates a label to hold the state of the sensor and a timer to display how long the sensor has
    been on.
    """

    critical_global = kivy.properties.BooleanProperty()
    fault = kivy.properties.BooleanProperty()
    parameter = kivy.properties.StringProperty()
    sensor_id = kivy.properties.NumericProperty()
    state = kivy.properties.BooleanProperty()

    def __init__(self, reading, **kwargs):
        super(SensorDigital, self).__init__(**kwargs)
        self.critical_global = int(reading['critical_global'])
        self.fault = False
        self.parameter = reading['parameter']
        self.sensor_id = reading['sensor_id']
        self.unit = ''

    def update(self, new_state):
        if new_state == '1':
            self.state = False
            self.fault = True
        else:
            self.state = True
            self.fault = False


class SensorAnalog(kivy.event.EventDispatcher):
    """
    This is the class for the sensor's state.  It's a container class to provide methods for updating the state of a sensor item.
    """

    critical_global = kivy.properties.BooleanProperty()
    fault = kivy.properties.BooleanProperty()
    parameter = kivy.properties.StringProperty()
    sensor_id = kivy.properties.NumericProperty()
    unit = kivy.properties.StringProperty()
    state = kivy.properties.NumericProperty()

    def __init__(self, reading, **kwargs):
        super(SensorAnalog, self).__init__(**kwargs)
        self.critical_global = int(reading['critical_global'])
        self.fault = False
        self.parameter = reading['parameter']
        self.sensor_id = int(reading['sensor_id'])
        self.threshold = reading['threshold']
        self.unit = reading['unit']

    def update(self, new_state):
        self.state = float(new_state)
        if self.state < float(self.threshold):
            self.fault = True
        else:
            self.fault = False


class SensorError(Exception):
    """
    This is the exception raised when there's an error passed on from a connected Arduino.

    Attributes:
        reading -- The reading from the Arduino that contained the error.
    """

    def __init__(self, reading):
        self.reading = reading
        self.message = "Error with sensor " + reading['panel'] + "_" + reading['sensor']
