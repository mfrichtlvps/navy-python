import logging

import kivy.logger

def change_color(new_color):
    # This helper function changes colors of tabs and sensor items.
    if new_color == 'normal':
        return([0, 1, 0, 1])
    elif new_color == 'warning':
        return([1, 1, 0, 1])
    elif new_color == 'error':
        return([1, 0, 0, 1])
    else:
        return([1, 1, 1, 1])

def setup_logger(logger_name, log_file, level):
    # Helper function to create loggers for tests.
    kivy.logger.Logger.info("Creating logger for [{0}].".format(logger_name))
    test_logger = logging.getLogger(logger_name)
    formatter = logging.Formatter('%(asctime)s : %(message)s')
    fileHandler = logging.FileHandler(log_file, mode='w')
    fileHandler.setFormatter(formatter)

    test_logger.setLevel(level)
    test_logger.addHandler(fileHandler)
    return(test_logger)
