"""
This file processes the DFT batch files from the Positector gage.
It uses only the files that are created by the Positector.  It creates a .csv
file for each batch and a master .csv for all readings in the parent diretory.
If the .csv already exists, it is overwritten.

DO NOT EDIT THE .CSV DIRECTLY.  IT WILL BE OVERWRITTEN BY THIS PROGRAM.
"""

from csv import reader, writer
from os import chdir, getcwd, listdir, pardir
from os.path import isdir, isfile, join
from tkinter import Tk
from tkinter.filedialog import askdirectory

class Batch:
    """
    This is the class for each DFT batch.
    """

    def __init__(self, batch_path, master_file):

        def get_batch_info(self):
            with open(self.batch_folder + '\header.txt', 'r') as file:
                lines = file.readlines()
            return [lines[1][12:-1], lines[2][10:-1]]

        def make_csv(self):
            with open(self.batch_folder + '/readings.csv', 'w') as w, \
                 open(self.batch_folder + '/readings.txt', 'r') as r, \
                 open(self.master_file, 'a') as m:
                write_batch = writer(w,
                                    dialect='excel',
                                    delimiter=',',
                                    lineterminator='\n')
                write_master = writer(m,
                                     dialect='excel',
                                     delimiter=',',
                                     lineterminator='\n')
                read = reader(r, dialect='excel')
                write_batch.writerow(['set_no', 'index', 'timestamp',
                                     'thickness', 'status', 'substrate'])
                next(r)
                for row in read:
                    master_row = [self.batch_name, self.batch_sub_folder]
                    master_row += row
                    write_batch.writerow(row)
                    write_master.writerow(master_row)

        self.master_file = master_file
        self.batch_folder = batch_path
        self.batch_sub_folder = batch_path.split('/')[-1]
        self.batch_name, self.time_stamp = get_batch_info(self)

        print('Initializing batch ' + self.batch_name + ' in')
        print(self.batch_folder)
        make_csv(self)


def make_master(parent_folder):
    """
    This code initializes the master .csv file that will contain all readings in
    the parent folder that was selected.
    """

    file_name = parent_folder + '/all_readings.csv'
    with open(file_name,'wt') as master_csv:
        master_file=writer(master_csv,dialect='excel', lineterminator='\n')
        master_file.writerow(['batch_name', 'directory', 'set_no', 'index',
                              'timestamp', 'thickness', 'status', 'substrate'])
    return(file_name)


def make_files(parent_folder, master_file):
    """
    This is what checks the directory(ies) and writes the data from the text
    files to masterData.csv.
    """

    for item in listdir(parent_folder):
        item_path = parent_folder + '/' + item
        if isdir(item_path):
            if isfile(item_path + '/header.txt'):
                Batch(item_path, master_file)


def main():
    """
    The code below allows the user to execute dft_gage.py.  It assumes that the
    folder in which the python script resides is the parent folder for all DFT
    batches.
    """

    root = Tk()
    root.withdraw()
    parent_folder = askdirectory()
    master_file = make_master(parent_folder)
    make_files(parent_folder, master_file)


main()
