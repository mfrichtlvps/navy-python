"""
This application is for monitoring the Auto Tech Standard Salt Spray cabinets
in the Coatings and Corrosion lab at NSWC Carderock.
"""

import kivy.app
import kivy.clock
import kivy.config
from kivy.logger import Logger
import kivy.properties
import kivy.uix.boxlayout
import kivy.uix.button
import kivy.uix.dropdown
import kivy.uix.label
import kivy.uix.tabbedpanel
import kivy.uix.textinput
from kivy.uix.widget import Widget

import datetime
import functools
import logging
import pathlib
import serial
import serial.tools.list_ports
import serial.serialutil
import time

import helpers
import sensors
import test_classes


class MonitorPanel(kivy.uix.tabbedpanel.TabbedPanel):
    """
    This is the container for the entire application.  It contains the top-level
    commands and the loop listening for serial communication from the Arduino.
    It handles the creation of all widgets by dispatching the appropriate
    methods for each class.
    """

    def __init__(self, **kwargs):
        super(MonitorPanel, self).__init__(**kwargs)
        Logger.debug("APP: {0} - Initializing application.".format(time.strftime('%x %X')))
        self.serial_refresh_time = 5 # 5 second refresh interval
        self.find_controllers(0) # The 0 doesn't do anything here.
        self.dispatcher = MonitorPanelDispatcher()
        self.dispatcher.bind(global_faults=self.handle_global_faults)
        self.sensors = {}
        # Next two initialize the interface without a default tab
        self.do_default_tab = False
        self.clear_tabs()
        # Initialize the menu tab
        self.menu = MenuPanelItem(id='menu', text='MENU')
        #self.add_widget(self.menu)

    def find_controllers(self, dt):
        """
        This function searches for Arduinos.  The dt argument is to enable
        scheduling of callbacks via Kivy.
        """

        Logger.debug("COMM: {0} - Searching for connected Arduinos.".format(time.strftime('%x %X')))
        self.controllers = []
        for device in serial.tools.list_ports.comports():
            if 'Arduino' in device.description:
                self.controllers.append(device)
        if len(self.controllers) > 0:
            self.open_connections()
        else:
            kivy.clock.Clock.schedule_once(self.find_controllers, 300)
            Logger.exception("COMM: {0} - No Arduinos found. Sleeping 5 minutes.".format(time.strftime('%x %X')))

    def my_callback(self, dt):
        try:
            for device in self.controllers:
                while device.serial.inWaiting() > 0:
                    received = str(device.serial.readline()).split("'")
                    message = received[1].split("_")
                    print(message)
                    if message[0] == 'N' or len(message) < 4:
                        pass
                    elif message[0] == 'I':
                        self.create_sensor(message[1:-1])
                    elif message[0] == 'U':
                        self.update_sensor(message[1:-1])
                if len(self.sensors) == 0:
                    self.initialize_sensors()
                else:
                    device.serial.write(b'U')

        except (AttributeError, serial.SerialException, TypeError):
            kivy.clock.Clock.schedule_once(self.find_controllers, 300)
            Logger.exception("COMM: {0} - No connected Arduinos found.".format(time.strftime('%x %X')))
            return(False)

        except serial.serialutil.SerialTimeoutException:
            kivy.clock.Clock.schedule_once(self.find_controllers, 300)
            Logger.exception("COMM: {0} - Write timeout.".format(time.strftime('%x %X')))

        except sensors.SensorError as error:
            print(error.message)

    def open_connections(self):
        try:
            for device in self.controllers:
                device.serial = serial.Serial(device.device, 115200, timeout=1)
                device.serial.reset_input_buffer()
                device.serial.reset_output_buffer()
            kivy.clock.Clock.schedule_interval(self.my_callback, self.serial_refresh_time)
            Logger.debug("COMM: {0} - {1} Connected Arduino(s) found.".format(time.strftime('%x %X'), len(self.controllers)))

        except TypeError:
            kivy.clock.Clock.schedule_once(self.find_controllers, 300)
            Logger.exception("COMM: {0} - No Arduinos found. Sleeping 5 minutes".format(time.strftime('%x %X')))

        except serial.SerialException:
            kivy.clock.Clock.schedule_once(self.find_controllers, 300)
            Logger.exception("COMM: {0} - Cannot open communication port(s).".format(time.strftime('%x %X')))

    def initialize_sensors(self):
        """
        This function (re)initializes all connected sensors.  It's called when
        the application receives an update from an unknown sensor or when the
        application doesn't know about any sensors at all.
        """
        for device in self.controllers:
            write_success = 0
            while write_success < 1:
                write_success = device.serial.write(b'I')
            kivy.clock.Clock.schedule_once(self.my_callback, self.serial_refresh_time)

    def handle_global_faults(self, dispatcher, global_faults):
        if self.dispatcher.global_faults > 0:
            for panel in self.ids.values():
                panel.dispatcher.global_fault = True
        else:
            for panel in self.ids.values():
                panel.dispatcher.global_fault = False

    def handle_sensor_faults(self, sensor, fault):
        if sensor.critical_global:
            if fault:
                self.dispatcher.global_faults += 1
                Logger.exception("FAULT: {0} - Global fault detected.".format(time.strftime('%x %X')))
            else:
                self.dispatcher.global_faults -= 1

    def create_sensor(self, reading):
        new_sensor = {}
        new_sensor['panel'] = reading[0]
        new_sensor['sensor_type'] = reading[1]
        new_sensor['sensor_id'] = reading[2]
        new_sensor['parameter'] = reading[3]
        new_sensor['measurement'] = reading[4]
        new_sensor['threshold'] = reading[5]
        new_sensor['unit'] = reading[6]
        new_sensor['critical_global'] = reading[7]
        self.sensors[new_sensor['sensor_id']] = new_sensor
        this_panel = self.get_or_create_panel_item(new_sensor['panel'])
        this_sensor = this_panel.create_sensor(new_sensor)
        this_sensor.sensor.bind(fault=self.handle_sensor_faults)
        if this_sensor.sensor.critical_global:
            self.dispatcher.global_faults += this_sensor.sensor.fault

    def update_sensor(self, reading):
        try:
            this_sensor = self.sensors[reading[0]]
            this_panel = self.get_or_create_panel_item(this_sensor['panel'])
            this_panel.update_sensor(reading)

        except KeyError:
            Logger.warning("OBJECTS: {0} - Update for unknown sensor [{1}] received. Reinitializing".format(time.strftime('%X %x'), reading[0]))
            self.initialize_sensors()

        except:
            Logger.warning("COMM: {0} - Unexpected sensor update error. Ignoring last message.".format(time.strftime('%x %x')))

    def get_or_create_panel_item(self, panel_name):
        try:
            this_panel = self.ids[panel_name]

        except KeyError:
            this_panel = MonitorPanelItem(id=panel_name, text=panel_name)
            self.ids[panel_name] = this_panel
            self.add_widget(this_panel)
            if "SERVICES" or "COMMON" not in this_panel.id:
                self.menu.add_test_container(this_panel)

        finally:
            return(this_panel)


class MonitorPanelDispatcher(kivy.event.EventDispatcher):
    """
    This is the dispatcher for handling the highest level events that need to
    be pushed to all panel items.
    """

    global_faults = kivy.properties.NumericProperty()

    def __init__(self, **kwargs):
        super(MonitorPanelDispatcher, self).__init__(**kwargs)
        self.global_faults = 0


class MonitorPanelItem(kivy.uix.tabbedpanel.TabbedPanelItem):
    """
    This is the basic class for a tabbed panel item, which is for the services
    tab and a tab for each cabinet.  This class will hold only those properties
    that are constant amongst both types of tabbed panels (e.g. color).
    """

    def __init__(self, **kwargs):
        super(MonitorPanelItem, self).__init__(**kwargs)
        Logger.info("OBJECTS: {0} - Adding new panel [{1}]".format(time.strftime('%X %x'), self.id))
        self.background_color = helpers.change_color('normal')
        self.tests_running = True

        self.dispatcher = MonitorPanelItemDispatcher()
        self.dispatcher.bind(global_fault=self.handle_global_faults)
        self.dispatcher.bind(faults=self.handle_panel_faults)

        self.layout = kivy.uix.gridlayout.GridLayout(cols=2)
        self.add_widget(self.layout)
        self.layout.add_widget(self.add_test_widget())
        self.tests = {}

        # Setup the application logger for condition logging
        self.logger = helpers.setup_logger(self.id, './logs/{0}-{1}-log.txt'.format(datetime.date.today(), self.id), level=logging.INFO)
        kivy.clock.Clock.schedule_interval(self.write_log, 900)

    def add_test_widget(self):
        add_test_layout = kivy.uix.gridlayout.GridLayout(cols=2)
        add_test_prompt = kivy.uix.label.Label(text="Create test in {0}\nusing the white box".format(self.text))
        add_test_layout.add_widget(add_test_prompt)
        add_test_input = kivy.uix.textinput.TextInput(multiline=False)
        add_test_layout.add_widget(add_test_input)
        add_test_input.bind(on_text_validate=self.on_enter)
        return(add_test_layout)

    def on_enter(self, value):
        self.create_test(test_classes.EnvironmentTest(self.text, value.text, rows=2))

    def cancel_test(self, instance):
        self.layout.remove_widget(instance.test)
        self.tests.pop(instance.test.name)

    def create_test(self, test):
        self.tests[test.name] = test
        self.layout.add_widget(test)
        self.cancel_button = kivy.uix.button.Button(text="Cancel Test")
        self.cancel_button.test = test
        self.cancel_button.bind(on_press=self.cancel_test)
        test.add_widget(self.cancel_button)
        if self.dispatcher.faults > 0 or self.dispatcher.global_fault:
            self.tests_running = False
            test.stop_test()
        test.write_log_header(self)
        test.write_log(self, 0.5)
        kivy.clock.Clock.schedule_interval(functools.partial(test.write_log, self), 900)

    def create_sensor(self, reading):
        sensor_id = reading['sensor_id']
        try:
            this_sensor = self.ids[sensor_id]
            this_sensor.update(reading['measurement'])

        except KeyError:
            Logger.info("OBJECTS: {0} - Adding sensor for [{1}] to panel [{2}]".format(time.strftime('%x %X'), reading['parameter'], self.text))
            this_sensor = sensors.SensorItem(reading)
            self.ids[sensor_id] = this_sensor
            self.layout.add_widget(this_sensor)
            this_sensor.sensor.bind(fault=self.handle_sensor_faults)
            if this_sensor.sensor.fault:
                self.dispatcher.faults += 1
            self.write_log_header()
            self.write_log(1)
            return(this_sensor)

        except sensors.SensorError as error:
            print(error.message)

    def fetch_sensor(self, reading):
        return(self.ids[reading[0]])

    def handle_global_faults(self, dispatcher, fault):
        if fault:
            self.stop_tests()
        else:
            if dispatcher.faults == 0:
                self.start_tests()

    def handle_panel_faults(self, dispatcher, faults):
        if dispatcher.faults > 0:
            self.background_color = helpers.change_color('error')
            self.tests_running = False
            for test in self.tests.values():
                Logger.debug("TESTS: {0} Stopping [{1}] in [{2}].".format(time.strftime('%x %X'), test.name, self.id))
                test.stop_test()
        else:
            self.background_color = helpers.change_color('normal')
            self.tests_running = True
            for test in self.tests.values():
                test.start_test()
                Logger.debug("TESTS: {0} Starting [{1}] in [{2}].".format(time.strftime('%x %X'), test.name, self.id))

    def handle_sensor_faults(self, sensor, fault):
        if fault:
            self.dispatcher.faults += 1
        else:
            self.dispatcher.faults -= 1

    def start_tests(self):
        # Wrapper function to start all tests
        for test in self.tests.values():
            test.start_test()

    def stop_tests(self):
        # Wrapper function to stop all tests
        for test in self.tests.values():
            test.stop_test()

    def write_log(self, dt):
        log = ""
        for sensor in self.ids.values():
            log += "{0}, ".format(sensor.sensor.state)
        self.logger.info(log)

    def write_log_header(self):
        log = ""
        for sensor in self.ids.values():
            log += "{0} ({1}), ".format(sensor.sensor.parameter, sensor.sensor.unit)
        self.logger.info(log)

    def update_sensor(self, reading):
        this_sensor = self.fetch_sensor(reading)
        this_sensor.update(reading[1])


class MonitorPanelItemDispatcher(kivy.event.EventDispatcher):
    """
    Event dispatcher for monitoring state changes with monitor panel items.
    """

    faults = kivy.properties.NumericProperty()
    global_fault = kivy.properties.BooleanProperty()

    def __init__(self, **kwargs):
        super(MonitorPanelItemDispatcher, self).__init__(**kwargs)
        self.faults = 0
        self.global_fault = False


class MenuPanelItem(kivy.uix.tabbedpanel.TabbedPanelItem):
    """
    This is the tabbed panel item that will hold buttons for the user to
    interact with the application.  Not currently in use in the application.
    """

    def __init__(self, **kwargs):
        super(MenuPanelItem, self).__init__(**kwargs)
        self.layout = kivy.uix.boxlayout.BoxLayout()
        self.add_widget(self.layout)

    def add_test_container(self, panel):
        btn = kivy.uix.button.Button(text="Start a test in {0}".format(panel.id))
        btn.bind(on_release=test_classes.EnvironmentTest(panel))
        self.layout.add_widget(btn)

class CabinetApp(kivy.app.App):
    def __init__(self, **kwargs):
        super(CabinetApp, self).__init__(**kwargs)
        Logger.debug("APP: {0} - Test log before initializing main application.".format(time.strftime('%X %x')))

    def build(self):
        monitor_app = MonitorPanel()
        return monitor_app

if __name__ == '__main__':
    pathlib.Path("./logs/tests").mkdir(parents=True, exist_ok=True)
    kivy.config.Config.set('kivy', 'log_level', 'debug')
    kivy.config.Config.write()
    CabinetApp().run()
