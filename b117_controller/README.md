# Environmental Monitoring application

This application is designed to enable the integration of arbitrary
environmental sensors.  The sensors are integrated via an Arduino (or similar)
board that utilizes a standardized communication syntax to relay information
to a python program on a host computer.

## Requirements for basic use

* Arduino and some support environmental sensors.
* Deployed executable running the python-based GUI.

## Development requirements

* Custom Arduino library (integration with others on-going)
* Python 3.5+
* Python Kivy module and it's requirements
* Pyinstaller for executable compilation, with the applications .spec file
